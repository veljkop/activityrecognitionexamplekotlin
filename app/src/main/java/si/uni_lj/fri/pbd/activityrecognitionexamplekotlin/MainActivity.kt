package si.uni_lj.fri.pbd.activityrecognitionexamplekotlin

import android.Manifest
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionRequest
import com.google.android.gms.location.DetectedActivity
import si.uni_lj.fri.pbd.activityrecognitionexamplekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
        const val TAG_REC = "ActivityTransRep"
        const val REQUEST_ID_ACTIVITY_PERMISSIONS = 42
        const val TRANSITION_RECEIVER_ACTION = "si.uni_lj.fri.pbd.activityrecognitionexamplekotlin.RESULT_RECEIVE"
        const val TRANSITIONS_TEXT = "transitions_text"
    }

    private lateinit var binding: ActivityMainBinding

    lateinit var pendingIntent: PendingIntent
    lateinit var receiver: ActivityTransitionReportReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {setupPermissions()} else {
            setActivityTransitionDetection()
        }
    }

    override fun onStart() {
        super.onStart()

        receiver = ActivityTransitionReportReceiver()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            registerReceiver(receiver, IntentFilter(TRANSITION_RECEIVER_ACTION), Context.RECEIVER_NOT_EXPORTED)
            Log.d(TAG, "Receiver registered - api O")
        } else {
            registerReceiver(receiver, IntentFilter(TRANSITION_RECEIVER_ACTION))
            Log.d(TAG, "Receiver registered")
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION)
            != PackageManager.PERMISSION_GRANTED){

            Log.d(TAG, "Permissions not granted, will ask")

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACTIVITY_RECOGNITION )) {

                Log.d(TAG, "Should show rationale")

                val builder = AlertDialog.Builder(this)
                with(builder) {
                    setMessage("permission required for activity recognition")
                    setTitle("Permission required")
                    setPositiveButton("OK") { p0, p1 -> makeRequest() }
                }

                val dialog = builder.create()
                dialog.show()
            } else {
                makeRequest()
            }
        } else {
            Log.d(TAG, "Permission granted")

            setActivityTransitionDetection()

        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun makeRequest() {
        Log.d(TAG, "Request permission")
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.ACTIVITY_RECOGNITION),
            REQUEST_ID_ACTIVITY_PERMISSIONS)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_ID_ACTIVITY_PERMISSIONS) {
            if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permission denied")
            } else {
                Log.d(TAG, "Permission granted")
                setActivityTransitionDetection()
            }
        }
    }

    override fun onStop() {
        super.onStop()

        // TODO: remove activity recognition sensing
        val task = ActivityRecognition.getClient(this)
            .removeActivityTransitionUpdates(pendingIntent)
        task.addOnSuccessListener{pendingIntent.cancel()}

        unregisterReceiver(receiver)
    }



    private fun setActivityTransitionDetection() {
        Log.d(TAG, "setActivityTransitionDetection")

        // TODO: list transitions we want to detect
        val transitions = mutableListOf<ActivityTransition>()

        transitions += ActivityTransition.Builder()
            .setActivityType(DetectedActivity.RUNNING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build()

        transitions += ActivityTransition.Builder()
            .setActivityType(DetectedActivity.RUNNING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build()

        transitions += ActivityTransition.Builder()
            .setActivityType(DetectedActivity.WALKING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build()

        transitions += ActivityTransition.Builder()
            .setActivityType(DetectedActivity.WALKING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build()

        transitions += ActivityTransition.Builder()
            .setActivityType(DetectedActivity.STILL)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build()

        transitions += ActivityTransition.Builder()
            .setActivityType(DetectedActivity.STILL)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build()


        // TODO: define a request
        val request = ActivityTransitionRequest(transitions)
        val intent = Intent(this, DetectedActivitiesBroadcastReceiver::class.java)

        pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_MUTABLE)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_MUTABLE)
//        } else {
//            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
//        }

        // TODO: subscribe to transition updates
        val task = ActivityRecognition.getClient(this)
            .requestActivityTransitionUpdates(request, pendingIntent)

        task.addOnSuccessListener {
            Log.d(TAG, "Transitions API registered")
            binding.tvStatus.text = "Subscribed to transition updates"
        }

        task.addOnFailureListener { e: Exception ->
            Log.d(TAG, "Transitions API could NOT be registered ${e.localizedMessage}")
        }



    }

    inner class ActivityTransitionReportReceiver: BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {

            intent?:return

            Log.d(TAG_REC, "onReceive")

            binding.tvStatus.text = intent.getStringExtra(TRANSITIONS_TEXT)
        }
    }
}