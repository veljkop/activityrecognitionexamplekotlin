package si.uni_lj.fri.pbd.activityrecognitionexamplekotlin

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionResult
import com.google.android.gms.location.DetectedActivity
import java.text.SimpleDateFormat
import java.util.*

class DetectedActivitiesBroadcastReceiver: BroadcastReceiver() {

    companion object {
        const val TAG = "DetectedActivitiesBR"
    }

    private fun activityToString(activity: Int): String {
        return when(activity) {
            DetectedActivity.STILL -> "STILL"
            DetectedActivity.WALKING -> "WALKING"
            DetectedActivity.RUNNING -> "RUNNING"
            DetectedActivity.IN_VEHICLE -> "IN VEHICLE"
            DetectedActivity.ON_BICYCLE -> "ON BICYCLE"
            DetectedActivity.ON_FOOT -> "ON FOOT"
            DetectedActivity.TILTING -> "TILTING"
            else -> "UNKNOWN"
        }
    }

    private fun transitionToString(transition: Int): String {
        return when(transition) {
            ActivityTransition.ACTIVITY_TRANSITION_ENTER -> "ENTER"
            ActivityTransition.ACTIVITY_TRANSITION_EXIT -> "EXIT"
            else -> "UNKNOWN"
        }
    }


    override fun onReceive(context: Context?, intent: Intent?) {

        Log.d(TAG, "onReceive")

        if (intent != null) {
            val result: ActivityTransitionResult? = ActivityTransitionResult.extractResult(intent)
            if (result != null) {

                val output = StringBuilder()

                for (event in result.transitionEvents) {
                    val info: String = "Transition: ${activityToString(event.activityType)}" +
                            " (${transitionToString(event.transitionType)}) " +
                            SimpleDateFormat("HH:mm:ss", Locale.US).format(Date())+"\n"
                    output.append(info)
                }

                val feedbackIntent = Intent(MainActivity.TRANSITION_RECEIVER_ACTION)
                feedbackIntent.putExtra(MainActivity.TRANSITIONS_TEXT, output.toString())
                context?.sendBroadcast(feedbackIntent)
            }
        }
    }

}
